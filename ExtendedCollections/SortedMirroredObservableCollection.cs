﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ExtendedCollections
{
    public class SortedMirroredObservableCollection<T> : ObservableCollection<T> where T : class, IComparable<T>
    {
        private ISortedList<T> MirroredList;

        public event CollectionReorderedDelegate CollectionReordered;

        private T LatestRemovedItem;
        private int LatestRemovedIndex;

        public SortedMirroredObservableCollection(ISortedList<T> list) : base(list)
        {
            MirroredList = list;
        }

        protected override void InsertItem(int index, T item)
        {
            MirroredList.Add(item);
            base.InsertItem(MirroredList.IndexOf(item), item);
            if (LatestRemovedItem == item && LatestRemovedIndex != index)
            {
                LatestRemovedItem = null;
                CollectionReordered.Invoke(item, LatestRemovedIndex, index);
            }
        }

        protected override void RemoveItem(int index)
        {
            LatestRemovedItem = base[index];
            LatestRemovedIndex = index;
            base.RemoveItem(index);
            MirroredList.RemoveAt(index);
        }

        protected override void ClearItems()
        {
            base.ClearItems();
            MirroredList.Clear();
        }

        public delegate void CollectionReorderedDelegate(T item, int oldIndex, int newIndex);
    }
}