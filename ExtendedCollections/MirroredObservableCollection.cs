﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ExtendedCollections
{
    public class MirroredObservableCollection<T> : ObservableCollection<T> where T : class
    {
        private IList<T> MirroredList;

        public event CollectionReorderedDelegate CollectionReordered;

        private T LatestRemovedItem;
        private int LatestRemovedIndex;

        public MirroredObservableCollection(IList<T> list) : base(list)
        {
            MirroredList = list;
        }

        protected override void InsertItem(int index, T item)
        {
            base.InsertItem(index, item);
            MirroredList.Insert(index, item);
            if (LatestRemovedItem == item && LatestRemovedIndex != index)
            {
                LatestRemovedItem = null;
                CollectionReordered.Invoke(item, LatestRemovedIndex, index);
            }
        }

        protected override void MoveItem(int oldIndex, int newIndex)
        {
            base.MoveItem(oldIndex, newIndex);
            var item = MirroredList[oldIndex];
            MirroredList.RemoveAt(oldIndex);
            if (oldIndex >= newIndex)
            {
                MirroredList.Insert(newIndex, item);
            }
            else
            {
                MirroredList.Insert(newIndex - 1, item);
            }
        }

        protected override void RemoveItem(int index)
        {
            LatestRemovedItem = base[index];
            LatestRemovedIndex = index;
            base.RemoveItem(index);
            MirroredList.RemoveAt(index);
        }

        protected override void SetItem(int index, T item)
        {
            base.SetItem(index, item);
            MirroredList[index] = item;
        }

        protected override void ClearItems()
        {
            base.ClearItems();
            MirroredList.Clear();
        }

        public delegate void CollectionReorderedDelegate(T item, int oldIndex, int newIndex);
    }
}