﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Text;

namespace ExtendedCollections
{
    public class SortedObservableCollection<T> : ObservableCollection<T> where T : class, IComparable<T>
    {
        private SortedList<T> InternalList { get; } = new SortedList<T>();

        public SortedObservableCollection()
        {
        }

        /// <summary>
        /// Removes all items from the collection.
        /// </summary>
        protected override void ClearItems()
        {
            base.ClearItems();
            InternalList.Clear();
        }

        /// <summary>
        /// Inserts an item into the collection at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="item" /> should be inserted.</param>
        /// <param name="item">The object to insert.</param>
        protected override void InsertItem(int index, T item)
        {
            InternalList.Add(item);
            base.InsertItem(InternalList.IndexOf(item), item);
        }

        /// <summary>
        /// Removes the item at the specified index of the collection.
        /// </summary>
        /// <param name="index">The zero-based index of the element to remove.</param>
        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
            InternalList.RemoveAt(index);
        }
    }
}