﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ExtendedCollections
{
    public interface ISortedList<T> : ICollection<T>, IEnumerable<T> where T : IComparable<T>
    {
        T this[int index] { get; }

        void AddRange(ICollection<T> collection);

        int IndexOf(T item);

        void RemoveAt(int index);
    }
}