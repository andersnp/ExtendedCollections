﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ExtendedCollections
{
    public class SortedList<T> : ISortedList<T> where T : class, IComparable<T>
    {
        private List<T> List { get; set; }

        public int Count => List.Count;

        public bool IsReadOnly => ((IList)List).IsReadOnly;

        public T this[int index] { get => List[index]; }

        public SortedList()
        {
            List = new List<T>();
        }

        public SortedList(IEnumerable<T> collection)
        {
            List = new List<T>(collection);
            List.Sort();
        }

        public SortedList(int capacity)
        {
            List = new List<T>(capacity);
        }

        public void Add(T item)
        {
            List.Add(item);
            List.Sort();
        }

        public void AddRange(ICollection<T> collection)
        {
            List.AddRange(collection);
            List.Sort();
        }

        public void Clear()
        {
            List.Clear();
        }

        public bool Contains(T item)
        {
            return List.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            List.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return List.GetEnumerator();
        }

        public int IndexOf(T item)
        {
            return List.BinarySearch(item);
        }

        public bool Remove(T item)
        {
            return List.Remove(item);
        }

        public void RemoveAt(int index)
        {
            List.RemoveAt(index);
        }
    }
}